import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            db: {},
        };
    }

    componentDidMount() {
        console.log('Hello, world!');
        fetch('https://gitlab.com/domtheporcupine/gltwttr/-/raw/master/database.json')
            .then((res) => {
                console.log(res)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    render() {
        return (<div>
            <p>Hello, world!</p>
        </div>);
    }
}

ReactDOM.render(<App />, document.getElementById('app'));